'use strict';

var postgres    = require('pg'),
    XoDsHandler = require('../');

require('./lib/chai');

describe('Bootstrap', function() {
    this.timeout(10000);

    it('should fail because DB not yet created', function(done) {
        var dbName           = require('./mocks/write-stores').dbName;
        var connectionString = require('./mocks/write-stores').stores.postgres;

        postgres.connect(connectionString, function callback(error, client, connectionDone) {
            if (error) {
                done(error);

                return console.error(error);
            }

            client.query(
                'DROP DATABASE IF EXISTS ' + dbName,
                function(err) {
                    if (err) {
                        done(err);
                        console.error('error running query', err);

                        return false;
                    }

                    var xoDsHandler = new XoDsHandler();

                    var config = {
                        'write-store': require('./mocks/write-stores').stores,
                        'read-store' : require('./mocks/read-stores').stores
                    };

                    return xoDsHandler.config(config)
                        .then(function() {
                            return xoDsHandler.pg(dbName, 'CREATE TABLE IF NOT EXISTS testtable (data jsonb NOT NULL)');
                        })
                        .should.eventually.be.rejectedWith('error: database "' + dbName + '" does not exist')
                        .then(done);
                });
        });
    });


    it('should setup the database', function(done) {
        var dbName           = require('./mocks/write-stores').dbName;
        var connectionString = require('./mocks/write-stores').stores.postgres;

        postgres.connect(connectionString, function callback(error, client, connectionDone) {
            if (error) {
                done(error);

                return console.error(error);
            }

            client.query(
                'DROP DATABASE IF EXISTS ' + dbName,
                function(err) {
                    if (err) {
                        done(err);
                        console.error('error running query', err);

                        return false;
                    }

                    client.query(
                        'CREATE DATABASE ' + dbName + ' WITH OWNER = postgres ENCODING = \'UTF8\' TABLESPACE = pg_default LC_COLLATE = \'en_US.UTF-8\' LC_CTYPE = \'en_US.UTF-8\' CONNECTION LIMIT = -1 TEMPLATE template0',
                        function(err) {
                            connectionDone();

                            if (err) {
                                done(err);
                                console.error('error running query', err);

                                return false;
                            }

                            done();
                        });
                });
        });
    });

    it('should configure xoDsHander', function() {
        var xoDsHandler = new XoDsHandler();

        var config = {
            'write-store': require('./mocks/write-stores').stores,
            'read-store' : require('./mocks/read-stores').stores
        };

        return xoDsHandler.config(config);
    });
});
