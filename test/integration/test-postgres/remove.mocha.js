'use strict';

var constants          = require('../../../src/helpers/constants'),
    XoDsHandler        = require('../../../'),
    XoRabbit           = require('xo-rabbit'),
    xoDsHandler        = new XoDsHandler(),
    xoRabbit           = new XoRabbit(),
    testDataSource     = {
        db   : require('../../mocks/write-stores').dbName,
        table: require('../../mocks/write-stores').tableName
    },
    tableHandler       = require('../../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

describe('PG: Remove - Emit message to Message Bus via XoRabbit', function() {
    this.timeout(5000);

    var resultRemoveRow = {
        name       : 'Test Row 2',
        location   : {
            city: 'Austin'
        },
        description: 'Test description text'
    };
    resultRemoveRow[identifierConstant] = 2;

    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler).then(function() {
                xoRabbit.config({
                    ssl                : false,
                    protocol           : 'amqp://',
                    user               : 'guest',
                    password           : 'guest',
                    server             : 'rabbitbox',
                    port               : 5672,
                    vhost              : '%2f',
                    heartbeat          : 2000,
                    autoAcknowledgement: false,
                    globalExchange     : 'local-global'
                });

                return xoRabbit.createQueue('Test-RemoveEvents').then(function success() {
                    return xoRabbit.connectQueue('Test-RemoveEvents', 'local-global', 'testrow-remove-integration');
                });
            });
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    it('should send and retrieve message to Message Bus via XoRabbit', function(done) {
        var removeData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-remove-integration',
            query     : {}
        };

        removeData.query[dataFieldConstant + '->>name'] = 'Test Row 2';

        xoDsHandler.remove(removeData).then(function() {
            setTimeout(function() {
                xoRabbit.get('Test-RemoveEvents').then(function(result) {
                    delete result.message['result-payload'][0].timestamp;

                    result.message.event.should.equal(removeData.event);
                    result.message['result-payload'][0].should.deep.equal(resultRemoveRow);
                    result.should.have.property('raw');

                    done();
                });
            }, 1000);
        });
    });
});
