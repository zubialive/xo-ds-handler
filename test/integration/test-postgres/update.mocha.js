'use strict';

var constants          = require('../../../src/helpers/constants'),
    XoDsHandler        = require('../../../'),
    XoRabbit           = require('xo-rabbit'),
    xoDsHandler        = new XoDsHandler(),
    xoRabbit           = new XoRabbit(),
    testDataSource     = {
        db   : require('../../mocks/write-stores').dbName,
        table: require('../../mocks/write-stores').tableName
    },
    tableHandler       = require('../../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

describe('PG: Update - Emit message to Message Bus via XoRabbit', function() {
    this.timeout(10000);

    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler).then(function() {
                xoRabbit.config({
                    ssl                : false,
                    protocol           : 'amqp://',
                    user               : 'guest',
                    password           : 'guest',
                    server             : 'rabbitbox',
                    port               : 5672,
                    vhost              : '%2f',
                    heartbeat          : 2000,
                    autoAcknowledgement: false,
                    globalExchange     : 'local-global'
                });

                return xoRabbit.createQueue('Test-UpdateEvents').then(function success() {
                    return xoRabbit.connectQueue('Test-UpdateEvents', 'local-global', 'testrow-updated-integration');
                });
            });
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultUpdateRow1 = {
        name       : 'Test Row 1 Updated',
        description: 'Test description text'
    };

    resultUpdateRow1[identifierConstant] = 1;

    var resultUpdateRow2 = {
        name       : 'Test Row 2 Updated',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };

    resultUpdateRow2[identifierConstant] = 2;

    it('should send and retrieve message to Message Bus via XoRabbit', function(done) {
        var updateData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-updated-integration',
            query     : {},
            payload   : {
                name: 'Test Row 1 Updated'
            }
        };

        updateData.query[dataFieldConstant + '->' + identifierConstant] = '1';

        xoDsHandler.update(updateData).then(function() {
            setTimeout(function() {
                xoRabbit.get('Test-UpdateEvents').then(function(result) {
                    delete result.message['result-payload'][0].timestamp;

                    result.message.event.should.deep.equal(updateData.event);
                    result.message['initial-payload'].should.deep.equal(updateData.payload);
                    result.message['result-payload'][0].should.deep.equal(resultUpdateRow1);
                    result.should.have.property('raw');

                    done();
                });
            }, 2000);
        });
    });

    it('should send and retrieve second message to Message Bus via XoRabbit', function(done) {
        var updateData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-updated-integration',
            query     : {},
            payload   : {
                name: 'Test Row 2 Updated'
            }
        };

        updateData.query[dataFieldConstant + '->' + identifierConstant] = '2';

        xoDsHandler.update(updateData).then(function() {
            setTimeout(function() {
                xoRabbit.get('Test-UpdateEvents').then(function(result) {
                    delete result.message['result-payload'][0].timestamp;

                    result.message.event.should.deep.equal(updateData.event);
                    result.message['initial-payload'].should.deep.equal(updateData.payload);
                    result.message['result-payload'][0].should.deep.equal(resultUpdateRow2);
                    result.should.have.property('raw');

                    done();
                });
            }, 1000);
        });
    });
});
