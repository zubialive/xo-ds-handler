/*jshint -W030 */
'use strict';

var constants          = require('../../../src/helpers/constants'),
    XoDsHandler        = require('../../../'),
    XoRabbit           = require('xo-rabbit'),
    xoDsHandler        = new XoDsHandler(),
    xoRabbit           = new XoRabbit(),
    testDataSource     = {
        db   : require('../../mocks/write-stores').dbName,
        table: require('../../mocks/write-stores').tableName
    },
    tableHandler       = require('../../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD;

describe('PG: Soft Remove - Emit message to Message Bus via XoRabbit', function() {
    this.timeout(5000);

    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler).then(function() {
                xoRabbit.config({
                    ssl                : false,
                    protocol           : 'amqp://',
                    user               : 'guest',
                    password           : 'guest',
                    server             : 'rabbitbox',
                    port               : 5672,
                    vhost              : '%2f',
                    heartbeat          : 2000,
                    autoAcknowledgement: false,
                    globalExchange     : 'local-global'
                });

                return xoRabbit.createQueue('Test-SoftRemoveEvents').then(function success() {
                    return xoRabbit.connectQueue('Test-SoftRemoveEvents', 'local-global', 'testrow-softremove-integration');
                });
            });
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    it('should send and retrieve message to Message Bus via XoRabbit', function(done) {
        var softRemoveData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-softremove-integration',
            native    : dataFieldConstant + '->\'location\'->>\'city\' = \'Austin\''
        };

        xoDsHandler.softRemove(softRemoveData).then(function() {
            setTimeout(function() {
                xoRabbit.get('Test-SoftRemoveEvents').then(function(result) {
                    result.message.event.should.deep.equal(softRemoveData.event);
                    result.message['initial-payload'].should.be.empty;
                    result.should.have.property('raw');

                    done();
                });
            }, 1000);
        });
    });
});
