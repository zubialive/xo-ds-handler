'use strict';

var constants          = require('../../../src/helpers/constants'),
    XoDsHandler        = require('../../../'),
    XoRabbit           = require('xo-rabbit'),
    XoLogger           = require('xo-logger'),
    xoLogger           = new XoLogger(),
    xoDsHandler        = new XoDsHandler(),
    xoRabbit           = new XoRabbit(),
    testDataSource     = {
        db   : require('../../mocks/write-stores').dbName,
        table: require('../../mocks/write-stores').tableName
    },
    tableHandler       = require('../../stubs/tableHandler')(testDataSource.table),
    identifierConstant = constants.WS_IDENTIFIER;

describe('PG: Create - Emit message to Message Bus via XoRabbit', function() {
    this.timeout(5000);

    before(function() {
        xoLogger.config({
            system : {
                environment    : 'dev',
                applicationName: 'test'
            }
        });

        return tableHandler.createTable(xoDsHandler).then(function() {
            xoRabbit.config({
                ssl                : false,
                protocol           : 'amqp://',
                user               : 'guest',
                password           : 'guest',
                server             : 'rabbitbox',
                port               : 5672,
                vhost              : '%2f',
                heartbeat          : 2000,
                autoAcknowledgement: false,
                globalExchange     : 'local-global'
            });

            return xoRabbit.createQueue('Test-CreateEvents')
                .then(function() {
                    return xoRabbit.createPublisher('local-global');
                })
                .then(function() {
                    return xoRabbit.connectQueue('Test-CreateEvents', 'local-global', 'testrow-added-integration');
                });
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    it('should send and retrieve message to Message Bus via XoRabbit', function(done) {
        var createData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-added-integration',
            payload   : {
                name       : 'Test Row 1',
                description: 'Test description text'
            }
        };

        createData.payload[identifierConstant] = 1;

        var functionHolder = {};

        functionHolder['hapi-calling-plugin'] = function() {
            xoDsHandler.create(createData).then(function() {
                setTimeout(function() {
                    xoRabbit.get('Test-CreateEvents').then(function(result) {
                        delete result.message['initial-payload'].timestamp;
                        delete result.message['result-payload'].timestamp;

                        delete createData.payload.timestamp;

                        result.message.event.should.deep.equal(createData.event);
                        result.message['initial-payload'].should.deep.equal(createData.payload);
                        result.message['result-payload'].should.deep.equal(createData.payload);
                        result.should.have.property('raw');

                        done();
                    });
                }, 1000);
            });
        };

        return functionHolder['hapi-calling-plugin']();
    });

    it('should send and retrieve second message to Message Bus via XoRabbit', function(done) {
        var createData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-added-integration',
            payload   : {
                name       : 'Test Row 2',
                description: 'Test description text'
            }
        };

        createData.payload[identifierConstant] = 2;

        xoDsHandler.create(createData).then(function() {
            setTimeout(function() {
                xoRabbit.get('Test-CreateEvents').then(function(result) {
                    delete result.message['initial-payload'].timestamp;
                    delete result.message['result-payload'].timestamp;

                    delete createData.payload.timestamp;

                    result.message.event.should.deep.equal(createData.event);
                    result.message['initial-payload'].should.deep.equal(createData.payload);
                    result.message['result-payload'].should.deep.equal(createData.payload);
                    result.should.have.property('raw');

                    done();
                });
            }, 1000);
        });
    });
});
