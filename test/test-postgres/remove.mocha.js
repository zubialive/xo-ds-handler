'use strict';

var Joi                = require('joi'),
    outputMapping      = require('../stubs/outputMapping'),
    constants          = require('../../src/helpers/constants'),
    XoDsHandler        = require('../../'),
    xoDsHandler        = new XoDsHandler(),
    testDataSource     = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler       = require('../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

require('../lib/chai');

describe('PG: Remove ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler);
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultRemoveRow = [];

    resultRemoveRow[0]                     = {
        name       : 'Test Row 2',
        location   : {
            city: 'Austin'
        },
        description: 'Test description text'
    };
    resultRemoveRow[0][identifierConstant] = 2;

    var resultRemoveMore = [];

    resultRemoveMore[0]                     = {
        name       : 'Test Row 3',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };
    resultRemoveMore[0][identifierConstant] = 3;

    var resultRemoveInexistentRow = [];

    /* Remove row */
    it('should respond: ' + JSON.stringify(resultRemoveRow), function() {
        var removeData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-remove',
            query        : {},
            outputMapping: outputMapping,
            outputSchema : {
                id         : Joi.number().required(),
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            logQuery     : true
        };

        removeData.query[dataFieldConstant + '->>name'] = 'Test Row 2';

        return xoDsHandler.remove(removeData).then(function(result) {
            delete result[0].timestamp;

            result.should.deep.equal(resultRemoveRow);
        });
    });

    /* Remove inexistent row */
    it('should respond: ' + JSON.stringify(resultRemoveInexistentRow), function() {
        var removeData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-remove',
            query     : {}
        };

        removeData.query[dataFieldConstant + '->>name'] = 'Test Row 10';

        return xoDsHandler.remove(removeData).should.eventually.deep.equal(resultRemoveInexistentRow);
    });

    /* Remove row if the 'name' contains '3' */
    it('should respond: ' + JSON.stringify(resultRemoveMore), function() {
        var removeData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-remove',
            native    : dataFieldConstant + '->>\'name\' LIKE \'%3%\''
        };

        return xoDsHandler.remove(removeData).then(function(result) {
            delete result[0].timestamp;

            result.should.deep.equal(resultRemoveMore);
        });
    });
});
