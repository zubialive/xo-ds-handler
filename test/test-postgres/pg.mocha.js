'use strict';

var constants          = require('../../src/helpers/constants'),
    XoDsHandler        = require('../../'),
    xoDsHandler        = new XoDsHandler(),
    testDataSource     = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler       = require('../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

require('../lib/chai');

describe('PG: PG Native Query ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler);
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultSelect = [{}];

    resultSelect[0][dataFieldConstant]                     = {
        name       : 'Test Row 1',
        description: 'Test description text'
    };
    resultSelect[0][dataFieldConstant][identifierConstant] = 1;

    /* Select query */
    it('should respond: ' + JSON.stringify(resultSelect), function() {
        var pgData = {
            dataSource: testDataSource.db,
            query     : 'SELECT * FROM ' + testDataSource.table + ' WHERE ' + dataFieldConstant + '->\'' + identifierConstant + '\'=\'1\''
        };

        return xoDsHandler.pg(pgData.dataSource, pgData.query).then(function(result) {
            delete result[0].data.timestamp;

            result.should.deep.equal(resultSelect);
        });
    });
});
