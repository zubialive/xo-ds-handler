'use strict';

var Joi                = require('joi'),
    outputMapping      = require('../stubs/outputMapping'),
    constants          = require('../../src/helpers/constants'),
    XoDsHandler        = require('../../'),
    xoDsHandler        = new XoDsHandler(),
    testDataSource     = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler       = require('../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

require('../lib/chai');

describe('PG: Read ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler);
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultRead = [];

    resultRead[0]                     = {
        name       : 'Test Row 2',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };
    resultRead[0][identifierConstant] = 2;

    resultRead[1]                     = {
        name       : 'Test Row 3',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };
    resultRead[1][identifierConstant] = 3;

    var resultReadDesc = [];

    resultReadDesc[0]                     = {
        name       : 'Test Row 3',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };
    resultReadDesc[0][identifierConstant] = 3;

    resultReadDesc[1]                     = {
        name       : 'Test Row 2',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };
    resultReadDesc[1][identifierConstant] = 2;

    var resultReadOne = [
        {
            id         : 2,
            name       : 'Test Row 2',
            description: 'Test description text',
            location   : {
                city: 'Austin'
            }
        }
    ];

    /* Read row using native query */
    it('should respond: ' + JSON.stringify(resultRead), function() {
        var readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {
                native: dataFieldConstant + '->\'location\'->>\'city\' = \'Austin\''
            },
            sort         : {},
            outputMapping: outputMapping,
            logQuery     : true
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant] = 'asc';

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultRead);
        });
    });

    /* Read row using native query */
    it('should respond: ' + JSON.stringify(resultReadOne), function() {
        var readData;

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            native       : dataFieldConstant + '->\'' + identifierConstant + '\' = \'2\'',
            outputMapping: outputMapping,
            outputSchema : {
                id         : Joi.number().required(),
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            }
        };

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;

            result.should.deep.equal(resultReadOne);
        });
    });

    /* Read row using object query */
    it('should respond: ' + JSON.stringify(resultReadDesc), function() {
        var queryCondition1 = {};
        var queryCondition2 = {};
        var readData;

        queryCondition1[dataFieldConstant + '->' + identifierConstant] = '2';
        queryCondition2[dataFieldConstant + '->location->>city']       = 'Austin';

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {
                or: [queryCondition1, queryCondition2]
            },
            limit        : 2,
            offset       : 0,
            sort         : {},
            outputMapping: outputMapping,
            logQuery     : true
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant] = 'desc';

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultReadDesc);
        });
    });

    /* Read row using 'and' operator */
    it('should respond: ' + JSON.stringify(resultReadOne), function() {
        var queryCondition1 = {};
        var queryCondition2 = {};
        var readData;

        queryCondition1[dataFieldConstant + '->' + identifierConstant] = '2';
        queryCondition2[dataFieldConstant + '->location->>city']       = 'Austin';

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {
                and: [queryCondition1, queryCondition2]
            },
            outputMapping: outputMapping
        };

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;

            result.should.deep.equal(resultReadOne);
        });
    });

    /* Read rows using 'gt' and 'lte' operators */
    it('should respond: ' + JSON.stringify(resultReadDesc), function() {
        var queryCondition1 = {};
        var queryCondition2 = {};
        var readData;

        queryCondition1[dataFieldConstant + '->' + identifierConstant] = {
            gt: '1'
        };
        queryCondition2[dataFieldConstant + '->' + identifierConstant] = {
            lte: '3'
        };

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {
                and: [queryCondition1, queryCondition2]
            },
            sort         : {},
            outputMapping: outputMapping
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant] = 'desc';

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultReadDesc);
        });
    });

    /* Read rows using 'gte' and 'lt' operators */
    it('should respond: ' + JSON.stringify(resultReadOne), function() {
        var queryCondition1 = {};
        var queryCondition2 = {};
        var readData;

        queryCondition1[dataFieldConstant + '->' + identifierConstant] = {
            gte: '2'
        };
        queryCondition2[dataFieldConstant + '->' + identifierConstant] = {
            lt: '3'
        };

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {
                and: [queryCondition1, queryCondition2]
            },
            sort         : {},
            outputMapping: outputMapping
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant] = 'desc';

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;

            result.should.deep.equal(resultReadOne);
        });
    });

    /* Read rows using 'not' operator */
    it('should respond: ' + JSON.stringify(resultReadDesc), function() {
        var readData;

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {},
            sort         : {},
            outputMapping: outputMapping
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant]  = 'desc';
        readData.query[dataFieldConstant + '->' + identifierConstant] = {
            not: '1'
        };

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultReadDesc);
        });
    });

    /* Read rows using 'like' and 'nlike' operator */
    it('should respond: ' + JSON.stringify(resultReadDesc), function() {
        var queryCondition1 = {};
        var queryCondition2 = {};
        var readData;

        queryCondition1[dataFieldConstant + '->location->>city'] = {
            like: 'Austin'
        };
        queryCondition2[dataFieldConstant + '->location->>city'] = {
            nlike: 'NYC'
        };

        readData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-read',
            query     : {
                and: [queryCondition1, queryCondition2]
            },
            sort      : {}
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant] = 'desc';

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultReadDesc);
        });
    });

    /* Read rows using 'regexp' operator */
    it('should respond: ' + JSON.stringify(resultReadDesc), function() {
        var readData;

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {},
            sort         : {},
            outputMapping: outputMapping
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant] = 'desc';
        readData.query[dataFieldConstant + '->location->>city']      = {
            regexp: '(Austin)'
        };

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultReadDesc);
        });
    });

    /* Read rows using 'jc' operator */
    it('should respond: ' + JSON.stringify(resultReadDesc), function() {
        var readData;

        readData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-read',
            query        : {},
            sort         : {},
            outputMapping: outputMapping
        };

        readData.sort[dataFieldConstant + '->' + identifierConstant] = 'desc';
        readData.query[dataFieldConstant + '->location']             = {
            jc: {
                city: 'Austin'
            }
        };

        return xoDsHandler.read(readData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultReadDesc);
        });
    });
});
