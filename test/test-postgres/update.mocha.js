'use strict';

var Joi                = require('joi'),
    inputMapping       = require('../stubs/inputMapping'),
    outputMapping      = require('../stubs/outputMapping'),
    constants          = require('../../src/helpers/constants'),
    XoDsHandler        = require('../../'),
    xoDsHandler        = new XoDsHandler(),
    testDataSource     = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler       = require('../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

require('../lib/chai');

describe('PG: Update ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler);
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultUpdateRow = [
        {
            name       : 'Test Row 1 Updated',
            description: 'Test description text'
        }
    ];

    resultUpdateRow[0][identifierConstant] = 1;

    var resultUpdateRows = [
        {
            name       : 'Test Row 2',
            description: 'Test description text',
            location   : {
                city : 'Austin',
                state: 'Texas'
            }
        },
        {
            name       : 'Test Row 3',
            description: 'Test description text',
            location   : {
                city : 'Austin',
                state: 'Texas'
            }
        }
    ];

    resultUpdateRows[0][identifierConstant] = 2;
    resultUpdateRows[1][identifierConstant] = 3;

    var resultUpdateNative = [
        {
            description: 'Test description text',
            id         : 2,
            location   : {
                city : 'Austin',
                state: 'Texas'
            },
            name       : 'Test Row 1 Updated natively'
        }
    ];

    var resultUpdateInexistentRow = [];

    /* Update row */
    it('should respond: ' + JSON.stringify(resultUpdateRow), function() {
        var updateData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-updated',
            query        : {},
            payload      : {
                name: 'Test Row 1 Updated'
            },
            payloadSchema: {
                name    : Joi.string().optional(),
                location: Joi.object().optional()
            },
            mapping      : inputMapping,
            outputMapping: outputMapping,
            outputSchema : {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            }
        };

        updateData.query[dataFieldConstant + '->' + identifierConstant] = '1';
        updateData.outputSchema[identifierConstant]                     = Joi.number().required();

        return xoDsHandler.update(updateData).then(function(result) {
            delete result[0].timestamp;

            result.should.deep.equal(resultUpdateRow);
        });
    });

    /* Update multiple rows */
    it('should respond: ' + JSON.stringify(resultUpdateRows), function() {
        var updateData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-updated',
            query        : {
                native: dataFieldConstant + '->\'location\'->>\'city\' = \'Austin\''
            },
            sort         : {},
            payload      : {
                location: {
                    city : 'Austin',
                    state: 'Texas'
                }
            },
            payloadSchema: {
                name    : Joi.string().optional(),
                location: Joi.object().optional()
            },
            mapping      : inputMapping,
            outputMapping: outputMapping,
            outputSchema : {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            logQuery     : true
        };

        updateData.sort[dataFieldConstant + '->>' + identifierConstant] = 'asc';
        updateData.outputSchema[identifierConstant]                     = Joi.number().required();

        return xoDsHandler.update(updateData).then(function(result) {
            delete result[0].timestamp;
            delete result[1].timestamp;

            result.should.deep.equal(resultUpdateRows);
        });
    });

    /* Update using native query */
    it('should respond: ' + JSON.stringify(resultUpdateNative), function() {
        var updateData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-updated',
            native    : dataFieldConstant + '->\'location\'->>\'city\' = \'Austin\'',
            limit     : 1,
            payload   : {
                name: 'Test Row 1 Updated natively'
            }
        };

        return xoDsHandler.update(updateData).then(function(result) {
            delete result[0].timestamp;

            result.should.deep.equal(resultUpdateNative);
        });
    });

    /* Update inexistent row */
    it('should respond: ' + JSON.stringify(resultUpdateInexistentRow), function() {
        var updateData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-updated',
            query     : {},
            payload   : {
                name: 'Test Row 10 Updated'
            }
        };

        updateData.query[dataFieldConstant + '->' + identifierConstant] = '10';

        return xoDsHandler.update(updateData).should.eventually.deep.equal(resultUpdateInexistentRow);
    });
});
