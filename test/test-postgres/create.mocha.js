'use strict';

var Joi                = require('joi'),
    inputMapping       = require('../stubs/inputMapping'),
    outputMapping      = require('../stubs/outputMapping'),
    constants          = require('../../src/helpers/constants'),
    XoDsHandler        = require('../../'),
    xoDsHandler        = new XoDsHandler(),
    testDataSource     = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler       = require('../stubs/tableHandler')(testDataSource.table),
    identifierConstant = constants.WS_IDENTIFIER;

require('../lib/chai');

describe('PG: Create ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler);
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultCreateFirst = {
        name       : 'Test Row 1',
        description: 'Test description text'
    };

    resultCreateFirst[identifierConstant] = 1;

    var resultCreateSecond = {
        name       : 'Test Row 2',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };

    resultCreateSecond[identifierConstant] = 2;

    var resultCreateThird = {
        name       : 'Test Row 3',
        description: 'Test description text',
        location   : {
            city: 'Austin'
        }
    };

    resultCreateThird[identifierConstant] = 3;

    var resultCreateDynamic   = 36;
    var resultCreateDuplicate = 'error: duplicate key value violates unique constraint \"' + testDataSource.table + '_' + identifierConstant + '\"';

    /* Create first row */
    it('should respond: ' + JSON.stringify(resultCreateFirst), function() {
        var createData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-added',
            payload      : {
                name       : 'Test Row 1',
                description: 'Test description text'
            },
            payloadSchema: {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            outputMapping: outputMapping,
            outputSchema : {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            logQuery     : true
        };

        createData.payload[identifierConstant]       = 1;
        createData.payloadSchema[identifierConstant] = Joi.number().required();
        createData.outputSchema[identifierConstant]  = Joi.number().required();

        return xoDsHandler.create(createData).then(function(result) {
            delete result.timestamp;

            result.should.deep.equal(resultCreateFirst);
        });
    });

    /* Create second row */
    it('should respond: ' + JSON.stringify(resultCreateSecond), function() {
        var createData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-added',
            payload      : {
                name       : 'Test Row 2',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                }
            },
            payloadSchema: {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }

            },
            mapping      : inputMapping,
            outputMapping: outputMapping,
            outputSchema : {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            }
        };

        createData.payload[identifierConstant]       = 2;
        createData.payloadSchema[identifierConstant] = Joi.number().required();
        createData.outputSchema[identifierConstant]  = Joi.number().required();

        return xoDsHandler.create(createData).then(function(result) {
            delete result.timestamp;

            result.should.deep.equal(resultCreateSecond);
        });
    });

    /* Create third row */
    it('should respond: ' + JSON.stringify(resultCreateThird), function() {
        var createData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-added',
            payload      : {
                name       : 'Test Row 3',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                }
            },
            payloadSchema: {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            outputMapping: outputMapping,
            outputSchema : {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            }
        };

        createData.payload[identifierConstant]       = 3;
        createData.payloadSchema[identifierConstant] = Joi.number().required();
        createData.outputSchema[identifierConstant]  = Joi.number().required();

        return xoDsHandler.create(createData).then(function(result) {
            delete result.timestamp;

            result.should.deep.equal(resultCreateThird);
        });
    });

    /* Create forth row with dynamically generated id */
    it('should respond: ' + resultCreateDynamic, function() {
        var createData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-added',
            payload      : {
                name       : 'Test Row 4',
                description: 'Test description text'
            },
            payloadSchema: {
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping
        };

        return xoDsHandler.create(createData).then(function(result) {
            (result[identifierConstant]).should.have.length(resultCreateDynamic);
        });
    });

    /* Create duplicate row - error */
    it('should respond with error: ' + JSON.stringify(resultCreateDuplicate), function() {
        var createData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-added',
            payload   : {
                name       : 'Test Row 1',
                description: 'Test description text'
            }
        };

        createData.payload[identifierConstant] = 1;

        return xoDsHandler.create(createData).should.eventually.be.rejectedWith(resultCreateDuplicate);
    });
});
