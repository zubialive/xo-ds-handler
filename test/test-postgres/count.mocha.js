'use strict';

var constants          = require('../../src/helpers/constants'),
    testDataSource     = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler       = require('../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../stubs/pgDataHandler')(testDataSource.table),
    XoDsHandler        = require('../../'),
    xoDsHandler        = new XoDsHandler(),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

require('../lib/chai');

describe('PG: Count ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler);
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultCountNone = 0;
    var resultCountOne  = 1;
    var resultCountTwo  = 2;

    /* Count rows */
    it('should respond: ' + JSON.stringify(resultCountOne), function() {
        var countData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            query     : {},
            logQuery  : true
        };

        countData.query[dataFieldConstant + '->' + identifierConstant] = '1';

        return xoDsHandler.count(countData).should.eventually.deep.equal(resultCountOne);
    });

    /* Count rows using native query */
    it('should respond: ' + JSON.stringify(resultCountOne), function() {
        var countData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            native    : dataFieldConstant + '->\'' + identifierConstant + '\' = \'1\''
        };

        return xoDsHandler.count(countData).should.eventually.deep.equal(resultCountOne);
    });

    /* Count rows where object in object */
    it('should respond: ' + JSON.stringify(resultCountTwo), function() {
        var countData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            query     : {}
        };

        countData.query[dataFieldConstant + '->location->>city'] = 'Austin';

        return xoDsHandler.count(countData).should.eventually.deep.equal(resultCountTwo);
    });

    /* Count rows where two conditions - no rows found */
    it('should respond: ' + JSON.stringify(resultCountNone), function() {
        var countData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            query     : {}
        };

        countData.query[dataFieldConstant + '->' + identifierConstant] = '2';
        countData.query[dataFieldConstant + '->location->>city']       = 'NYC';

        return xoDsHandler.count(countData).should.eventually.deep.equal(resultCountNone);
    });
});
