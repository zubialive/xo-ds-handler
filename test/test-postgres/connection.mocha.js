'use strict';

var constants         = require('../../src/helpers/constants'),
    XoDsHandler       = require('../../'),
    xoDsHandler       = new XoDsHandler(),
    testDataSource    = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler      = require('../stubs/tableHandler')(testDataSource.table),
    dataFieldConstant = constants.WS_DATA_FIELD;

require('../lib/chai');

describe('PG: Connection ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler);
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultReadWrongDatabase = 'Error: There is no configuration for database: typo';
    var resultReadWrongTable    = 'error: relation \"typo\" does not exist';
    var resultInvalidDataSource = 'Error: Unable to parse dataSource param';

    /* Create row using incorrect database name - error */
    it('should respond with error: ' + resultReadWrongDatabase, function() {
        var pgData = {
            dataSource: 'typo',
            query     : 'SELECT * FROM ' + testDataSource.table
        };

        return xoDsHandler.pg(pgData.dataSource, pgData.query).should.eventually.be.rejectedWith(resultReadWrongDatabase);
    });

    /* Create row using incorrect table name - error */
    it('should respond with error: ' + resultReadWrongTable, function() {
        var pgData = {
            dataSource: testDataSource.db,
            query     : 'SELECT * FROM typo'
        };

        return xoDsHandler.pg(pgData.dataSource, pgData.query).should.eventually.be.rejectedWith(resultReadWrongTable);
    });

    /* Create row using incorrect database and table name - error */
    it('should respond with error: ' + resultReadWrongDatabase, function() {
        var pgData = {
            dataSource: 'typo',
            query     : 'SELECT * FROM typo'
        };

        return xoDsHandler.pg(pgData.dataSource, pgData.query).should.eventually.be.rejectedWith(resultReadWrongDatabase);
    });

    /* Read row using invalid dataSource - error */
    it('should respond: ' + JSON.stringify(resultInvalidDataSource), function() {
        var readData = {
            dataSource: testDataSource.db,
            event     : 'testrow-read',
            query     : {
                native: dataFieldConstant + '->\'location\'->>\'city\' = \'Austin\''
            }
        };

        return xoDsHandler.read(readData).should.eventually.be.rejectedWith(resultInvalidDataSource);
    });
});
