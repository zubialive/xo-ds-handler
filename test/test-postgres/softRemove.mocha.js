'use strict';

var Joi                = require('joi'),
    outputMapping      = require('../stubs/outputMapping'),
    constants          = require('../../src/helpers/constants'),
    XoDsHandler        = require('../../'),
    xoDsHandler        = new XoDsHandler(),
    testDataSource     = {
        db   : require('../mocks/write-stores').dbName,
        table: require('../mocks/write-stores').tableName
    },
    tableHandler       = require('../stubs/tableHandler')(testDataSource.table),
    dataHandler        = require('../stubs/pgDataHandler')(testDataSource.table),
    dataFieldConstant  = constants.WS_DATA_FIELD,
    identifierConstant = constants.WS_IDENTIFIER;

require('../lib/chai');

describe('PG: Soft Remove ', function() {
    before(function() {
        return tableHandler.createTable(xoDsHandler).then(function() {
            return dataHandler.insert(xoDsHandler);
        });
    });

    after(function() {
        return tableHandler.dropTable(xoDsHandler);
    });

    /* Tests result objects */
    var resultSoftRemove = [
        {
            name       : 'Test Row 2',
            description: 'Test description text',
            location   : {
                city: 'Austin'
            },
            status     : 'deleted'
        },
        {
            name       : 'Test Row 3',
            description: 'Test description text',
            location   : {
                city: 'Austin'
            },
            status     : 'deleted'
        }
    ];

    resultSoftRemove[0][identifierConstant] = 2;
    resultSoftRemove[1][identifierConstant] = 3;

    var resultSoftRemoveInexistent = [];

    /* Soft remove rows */
    it('should respond: ' + JSON.stringify(resultSoftRemove), function() {
        var i;
        var softRemoveData = {
            dataSource   : testDataSource.db + '.' + testDataSource.table,
            event        : 'testrow-softremove',
            query        : {},
            sort         : {},
            outputMapping: outputMapping,
            outputSchema : {
                id         : Joi.number().required(),
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                status     : Joi.string().optional(),
                timestamp  : {
                    deletedAt: Joi.string().optional(),
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            logQuery     : true
        };

        softRemoveData.query[dataFieldConstant + '->location->>city']      = 'Austin';
        softRemoveData.sort[dataFieldConstant + '->' + identifierConstant] = 'asc';

        return xoDsHandler.softRemove(softRemoveData).then(function(result) {
            for (i = 0; i < result.length; i++) {
                result[i].timestamp.should.have.property('deletedAt');

                delete result[i].timestamp;
            }

            result.should.deep.equal(resultSoftRemove);
        });
    });

    /* Soft remove rows using native */
    it('should respond: ' + JSON.stringify(resultSoftRemove), function() {
        var i;
        var softRemoveData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-softremove',
            native    : dataFieldConstant + '->\'location\'->>\'city\' = \'Austin\''
        };

        return xoDsHandler.softRemove(softRemoveData).then(function(result) {
            for (i = 0; i < result.length; i++) {
                result[i].timestamp.should.have.property('deletedAt');

                delete result[i].timestamp;
            }

            result.should.deep.equal(resultSoftRemove);
        });
    });

    /* Soft remove rows while select fails */
    it('should respond: ' + JSON.stringify(resultSoftRemoveInexistent), function() {
        var i;
        var softRemoveData = {
            dataSource: testDataSource.db + '.' + testDataSource.table,
            event     : 'testrow-softremove',
            native    : dataFieldConstant + '->\'location\'->>\'city\' = \'NYC\''
        };

        return xoDsHandler.softRemove(softRemoveData).then(function(result) {
            for (i = 0; i < result.length; i++) {
                result[i].timestamp.should.have.property('deletedAt');

                delete result[i].timestamp.deletedAt;
            }

            result.should.deep.equal(resultSoftRemoveInexistent);
        });
    });
});
