'use strict';

var Joi          = require('joi'),
    inputMapping = require('../stubs/inputMapping');

var Constructor = function Constructor(dataSource) {
    this.index = dataSource.index || 'users';
    this.type  = dataSource.type || 'users';
};

Constructor.prototype.insert = function insert(instance) {
    var createDocuments = {
        dataSource   : this.index + '.' + this.type,
        payload      : [
            {
                id         : 1,
                name       : 'Test Row 1',
                description: 'Test description text',
                orderId    : 1
            },
            {
                id         : 2,
                name       : 'Test Row 2',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                },
                orderId    : 2
            },
            {
                id         : 3,
                name       : 'Test Row 3',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                },
                orderId    : 3
            }
        ],
        payloadSchema: {
            id         : Joi.number().required(),
            name       : Joi.string().optional(),
            description: Joi.string().optional(),
            location   : Joi.object().optional(),
            orderId    : Joi.number().optional(),
            timestamp  : {
                createdAt: Joi.string().optional(),
                updatedAt: Joi.string().optional()
            }
        },
        mapping      : inputMapping
    };

    return instance.index(createDocuments);
};

Constructor.prototype.removeAll = function removeAll(instance) {
    var deleteAllDocuments = {
        dataSource: this.index + '.' + this.type,
        query     : {
            match_all: {}
        }
    };

    return instance.unindex(deleteAllDocuments);
};

module.exports = function(dataSource) {
    return new Constructor(dataSource);
};
