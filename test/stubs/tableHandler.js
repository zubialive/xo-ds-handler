'use strict';

var constants = require('../../src/helpers/constants');

var Constructor = function Constructor(table) {
    this.table = table || require('../mocks/write-stores').tableName;
    this.db    = require('../mocks/write-stores').dbName;
};

Constructor.prototype.createTable = function createTable(instance) {
    var tableData = {
        dataSource: this.db,
        query     : 'DROP TABLE IF EXISTS ' + this.table + ';' + 'CREATE TABLE ' + this.table + ' (' + constants.WS_DATA_FIELD + ' jsonb); ' + 'CREATE UNIQUE INDEX ' + this.table + '_' + constants.WS_IDENTIFIER + ' ON ' + this.table + ' ((' + constants.WS_DATA_FIELD + '->>\'' + constants.WS_IDENTIFIER + '\'))'
    };

    return instance.pg(tableData.dataSource, tableData.query);
};

Constructor.prototype.dropTable = function dropTable(instance) {
    var tableData = {
        dataSource: this.db,
        query     : 'DROP TABLE ' + this.table
    };

    return instance.pg(tableData.dataSource, tableData.query);
};

module.exports = function(table) {
    return new Constructor(table);
};
