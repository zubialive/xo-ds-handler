'use strict';

var Promise            = require('bluebird'),
    Joi                = require('joi'),
    inputMapping       = require('../stubs/inputMapping'),
    outputMapping      = require('../stubs/outputMapping'),
    constants          = require('../../src/helpers/constants'),
    identifierConstant = constants.WS_IDENTIFIER;

var Constructor = function Constructor(table) {
    this.table = table || require('../mocks/write-stores').tableName;
    this.db    = require('../mocks/write-stores').dbName;
};

Constructor.prototype.insert = function insert(instance) {
    var createFirst = {
        dataSource   : this.db + '.' + this.table,
        event        : 'testrow-added',
        payload      : {
            name       : 'Test Row 1',
            description: 'Test description text'
        },
        payloadSchema: {
            name       : Joi.string().optional(),
            description: Joi.string().optional(),
            timestamp  : {
                createdAt: Joi.string().optional(),
                updatedAt: Joi.string().optional()
            }
        },
        mapping      : inputMapping,
        outputMapping: outputMapping,
        outputSchema : {
            name       : Joi.string().optional(),
            description: Joi.string().optional(),
            timestamp  : {
                createdAt: Joi.string().optional(),
                updatedAt: Joi.string().optional()
            }
        }
    };

    createFirst.payload[identifierConstant]       = 1;
    createFirst.payloadSchema[identifierConstant] = Joi.number().required();
    createFirst.outputSchema[identifierConstant]  = Joi.number().required();

    var createSecond = {
        dataSource   : this.db + '.' + this.table,
        event        : 'testrow-added',
        payload      : {
            name       : 'Test Row 2',
            description: 'Test description text',
            location   : {
                city: 'Austin'
            }
        },
        payloadSchema: {
            name       : Joi.string().optional(),
            description: Joi.string().optional(),
            location   : Joi.object().optional()

        },
        mapping      : inputMapping,
        outputMapping: outputMapping,
        outputSchema : {
            name       : Joi.string().optional(),
            description: Joi.string().optional(),
            location   : Joi.object().optional(),
            timestamp  : {
                createdAt: Joi.string().optional(),
                updatedAt: Joi.string().optional()
            }
        }
    };

    createSecond.payload[identifierConstant]       = 2;
    createSecond.payloadSchema[identifierConstant] = Joi.number().required();
    createSecond.outputSchema[identifierConstant]  = Joi.number().required();

    var createThird = {
        dataSource   : this.db + '.' + this.table,
        event        : 'testrow-added',
        payload      : {
            name       : 'Test Row 3',
            description: 'Test description text',
            location   : {
                city: 'Austin'
            }
        },
        payloadSchema: {
            name       : Joi.string().optional(),
            description: Joi.string().optional(),
            location   : Joi.object().optional()
        },
        mapping      : inputMapping,
        outputMapping: outputMapping,
        outputSchema : {
            name       : Joi.string().optional(),
            description: Joi.string().optional(),
            location   : Joi.object().optional(),
            timestamp  : {
                createdAt: Joi.string().optional(),
                updatedAt: Joi.string().optional()
            }
        }
    };

    createThird.payload[identifierConstant]       = 3;
    createThird.payloadSchema[identifierConstant] = Joi.number().required();
    createThird.outputSchema[identifierConstant]  = Joi.number().required();

    return Promise.all([
        instance.create(createFirst),
        instance.create(createSecond),
        instance.create(createThird)
    ]);
};

module.exports = function(table) {
    return new Constructor(table);
};
