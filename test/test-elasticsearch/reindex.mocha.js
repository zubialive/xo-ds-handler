'use strict';

var Joi            = require('joi'),
    inputMapping   = require('../stubs/inputMapping'),
    XoDsHandler    = require('../../'),
    xoDsHandler    = new XoDsHandler(),
    testDataSource = {
        index: require('../mocks/read-stores').index,
        type : require('../mocks/read-stores').type
    },
    dataHandler    = require('../stubs/esDataHandler')(testDataSource);

require('../lib/chai');

describe('ES: Reindex ', function() {
    before(function() {
        return dataHandler.insert(xoDsHandler);
    });

    after(function() {
        return dataHandler.removeAll(xoDsHandler);
    });

    /* Tests result objects */
    var resultReindex = {
        errors: false,
        items : [
            {
                update: {
                    _id     : '1',
                    _index  : testDataSource.index,
                    _type   : testDataSource.type,
                    _version: 2,
                    status  : 200
                }
            }
        ]
    };

    var resultReindexSecond = {
        errors: false,
        items : [
            {
                index: {
                    _id     : '2',
                    _index  : testDataSource.index,
                    _type   : testDataSource.type,
                    _version: 2,
                    status  : 200
                }
            }
        ]
    };

    var resultReindexRefresh = {
        errors: false,
        items : [
            {
                update: {
                    _id     : '1',
                    _index  : testDataSource.index,
                    _type   : testDataSource.type,
                    _version: 3,
                    status  : 200
                }
            }
        ]
    };

    var resultReindexRefreshSecond = {
        errors: false,
        items : [
            {
                update: {
                    _id     : '1',
                    _index  : testDataSource.index,
                    _type   : testDataSource.type,
                    _version: 4,
                    status  : 200
                }
            }
        ]
    };

    var resultReindexInexistent = {
        errors: true,
        items : [
            {
                update: {
                    _id   : '10',
                    _index: testDataSource.index,
                    _type : testDataSource.type,
                    error : 'DocumentMissingException[[users][-1] [users][10]: document missing]',
                    status: 404
                }
            }
        ]
    };

    /* Update document */
    it('should respond: ' + JSON.stringify(resultReindex), function() {
        var reindexData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            query        : {
                id: '1'
            },
            payload      : {
                name    : 'Test Row Updated',
                location: {
                    city: 'Austin'
                }
            },
            payloadSchema: {
                name     : Joi.string().optional(),
                location : Joi.object().optional(),
                timestamp: {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            replaceData  : false,
            logQuery     : true
        };

        return xoDsHandler.reindex(reindexData).then(function(result) {
            delete result.took;
            result.should.deep.equal(resultReindex);
        });
    });

    /* Update document withotu payload schema and mapping, using replace data */
    it('should respond: ' + JSON.stringify(resultReindexSecond), function() {
        var reindexData = {
            dataSource : testDataSource.index + '.' + testDataSource.type,
            query      : {
                id: '2'
            },
            payload    : {
                name    : 'Test Row Updated',
                location: {
                    city: 'Austin'
                }
            },
            replaceData: true
        };

        return xoDsHandler.reindex(reindexData).then(function(result) {
            delete result.took;
            result.should.deep.equal(resultReindexSecond);
        });
    });

    /* Update document with forced shard refresh*/
    it('should reindex with refresh set to true', function() {
        var reindexData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            query        : {
                id: '1'
            },
            payload      : {
                name    : 'Test Row Updated AGAIN',
                location: {
                    city: 'Austin'
                }
            },
            payloadSchema: {
                name     : Joi.string().optional(),
                location : Joi.object().optional(),
                timestamp: {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            replaceData  : false,
            logQuery     : true,
            refresh      : true
        };

        return xoDsHandler.reindex(reindexData).then(function(result) {
                delete result.took;
                result.should.deep.equal(resultReindexRefresh);

                return xoDsHandler.search({
                    dataSource: testDataSource.index + '.' + testDataSource.type,
                    query     : {
                        match: {
                            id: '1'
                        }
                    }
                });
            })
            .then(function(searchResult) {
                searchResult.hits.length.should.equal(1);
                searchResult.hits[0].name.should.equal('Test Row Updated AGAIN');
            });
    });

    /* Update document without forced shard refresh*/
    it('should reindex with refresh defaulted to false', function() {
        var reindexData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            query        : {
                id: '1'
            },
            payload      : {
                name    : 'Test Row Updated AGAINX2',
                location: {
                    city: 'Austin'
                }
            },
            payloadSchema: {
                name     : Joi.string().optional(),
                location : Joi.object().optional(),
                timestamp: {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            replaceData  : false,
            logQuery     : true
        };

        return xoDsHandler.reindex(reindexData).then(function(result) {
                delete result.took;
                result.should.deep.equal(resultReindexRefreshSecond);

                return xoDsHandler.search({
                    dataSource: testDataSource.index + '.' + testDataSource.type,
                    query     : {
                        match: {
                            id: '1'
                        }
                    }
                });
            })
            .then(function(searchResult) {
                searchResult.hits.length.should.equal(1);
                searchResult.hits[0].name.should.equal('Test Row Updated AGAIN');
            });
    });

    /* Update inexistent document */
    it('should respond: ' + JSON.stringify(resultReindexInexistent), function() {
        var reindexData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            query        : {
                id: '10'
            },
            payload      : {
                name    : 'Test Row Updated',
                location: {
                    city: 'Austin'
                }
            },
            payloadSchema: {
                name     : Joi.string().optional(),
                location : Joi.object().optional(),
                timestamp: {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            replaceData  : false,
            logQuery     : true
        };

        return xoDsHandler.reindex(reindexData).error(function(error) {
            var result = JSON.parse(error.message);
            delete result.took;
            result.should.deep.equal(resultReindexInexistent);
        });
    });
});
