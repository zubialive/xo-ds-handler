'use strict';

var outputMapping  = require('../stubs/outputMapping'),
    XoDsHandler    = require('../../'),
    xoDsHandler    = new XoDsHandler(),
    testDataSource = {
        index: require('../mocks/read-stores').index,
        type : require('../mocks/read-stores').type
    },
    dataHandler    = require('../stubs/esDataHandler')(testDataSource);

require('../lib/chai');

describe('ES: Connection ', function() {
    before(function() {
        return dataHandler.insert(xoDsHandler);
    });

    after(function() {
        return dataHandler.removeAll(xoDsHandler);
    });

    /* Tests result objects */
    var resultError             = 'Error: There is no configuration for elasticsearch: ' + testDataSource.index + 'typo';
    var resultInvalidDataSource = 'Error: Unable to parse dataSource param';

    /* Index typo - error */
    it('should respond: ' + JSON.stringify(resultError), function() {
        var searchData = {
            dataSource   : testDataSource.index + 'typo.' + testDataSource.type,
            query        : {
                match_all: {}
            },
            outputMapping: outputMapping
        };

        return xoDsHandler.search(searchData).should.eventually.be.rejectedWith(resultError);
    });

    /* Invalid dataSource param - error */
    it('should respond: ' + JSON.stringify(resultInvalidDataSource), function() {
        var searchData = {
            dataSource   : 'invalid-datasource',
            query        : {
                match_all: {}
            },
            outputMapping: outputMapping
        };

        return xoDsHandler.search(searchData).should.eventually.be.rejectedWith(resultInvalidDataSource);
    });
});
