'use strict';

var Joi            = require('joi'),
    outputMapping  = require('../stubs/outputMapping'),
    XoDsHandler    = require('../../'),
    xoDsHandler    = new XoDsHandler(),
    testDataSource = {
        index: require('../mocks/read-stores').index,
        type : require('../mocks/read-stores').type
    },
    dataHandler    = require('../stubs/esDataHandler')(testDataSource);

require('../lib/chai');

describe('ES: Search ', function() {
    this.timeout(5000);

    before(function(done) {
        dataHandler.insert(xoDsHandler);

        setTimeout(function() {
            done();
        }, 2000);
    });

    after(function() {
        return dataHandler.removeAll(xoDsHandler);
    });

    /* Tests result objects */
    var resultSearch = {
        hits : [
            {
                id         : '1',
                name       : 'Test Row 1',
                description: 'Test description text',
                orderId    : 1
            },
            {
                id         : '2',
                name       : 'Test Row 2',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                },
                orderId    : 2
            },
            {
                id         : '3',
                name       : 'Test Row 3',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                },
                orderId    : 3
            }
        ],
        total: 3
    };

    var resultOne = {
        hits : [
            {
                id         : '1',
                name       : 'Test Row 1',
                description: 'Test description text',
                orderId    : 1
            }
        ],
        total: 1
    };

    var resultTwo = {
        hits : [
            {
                id         : '3',
                name       : 'Test Row 3',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                },
                orderId    : 3
            },
            {
                id         : '2',
                name       : 'Test Row 2',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                },
                orderId    : 2
            }
        ],
        total: 2
    };

    var resultAggs = {
        aggregations: {
            my_agg: []
        },
        hits        : [
            {
                id         : '1',
                name       : 'Test Row 1',
                description: 'Test description text',
                orderId    : 1
            }
        ],
        total       : 1
    };

    /* Read all documents */
    it('should respond: ' + JSON.stringify(resultSearch), function() {
        var searchData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            query        : {
                match_all: {}
            },
            sort         : {
                orderId: 'asc'
            },
            outputMapping: outputMapping,
            logQuery     : true
        };

        return xoDsHandler.search(searchData).then(function(result) {
            for (var i in result.hits) {
                delete result.hits[i].timestamp;
            }

            result.should.deep.equal(resultSearch);
        });
    });

    /* Read one document */
    it('should respond: ' + JSON.stringify(resultOne), function() {
        var searchData = {
            dataSource  : testDataSource.index + '.' + testDataSource.type,
            query       : {
                match: {
                    id: '1'
                }
            },
            limit       : 1,
            offset      : 0,
            outputSchema: {
                id         : Joi.string().required(),
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                orderId    : Joi.number().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            }
        };

        return xoDsHandler.search(searchData).then(function(result) {
            delete result.hits[0].timestamp;

            result.should.deep.equal(resultOne);
        });
    });

    /* Read one filtered document */
    it('should respond: ' + JSON.stringify(resultOne), function() {
        var searchData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            query     : {
                filtered: {
                    query : {
                        match: {
                            id: '1'
                        }
                    },
                    filter: {
                        term: {
                            name: '1'
                        }
                    }
                }
            }
        };

        return xoDsHandler.search(searchData).then(function(result) {
            delete result.hits[0].timestamp;

            result.should.deep.equal(resultOne);
        });
    });

    /* Read one filtered document */
    it('should respond: ' + JSON.stringify(resultOne), function() {
        var searchData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            query     : {
                filtered: {
                    filter: {
                        term: {
                            id: '1'
                        }
                    }
                }
            }
        };

        return xoDsHandler.search(searchData).then(function(result) {
            delete result.hits[0].timestamp;

            result.should.deep.equal(resultOne);
        });
    });

    /* Read one document using filter */
    it('should respond: ' + JSON.stringify(resultOne), function() {
        var searchData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            filter    : {
                term: {
                    name: '1'
                }
            }
        };

        return xoDsHandler.search(searchData).then(function(result) {
            delete result.hits[0].timestamp;

            result.should.deep.equal(resultOne);
        });
    });

    /* Read documents using aggregation */
    it('should respond: ' + JSON.stringify(resultAggs), function() {
        var searchData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            query        : {
                match: {
                    id: '1'
                }
            },
            aggs         : {
                my_agg: {
                    terms: {
                        field: 'text'
                    }
                }
            },
            outputMapping: outputMapping
        };

        return xoDsHandler.search(searchData).then(function(result) {
            delete result.hits[0].timestamp;

            result.should.deep.equal(resultAggs);
        });
    });

    /* Read multiple documents */
    it('should respond: ' + JSON.stringify(resultTwo), function() {
        var removeData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            query     : {
                bool: {
                    should: [
                        {
                            match: {
                                name: {
                                    query   : 'Test Row 2',
                                    operator: 'and'
                                }
                            }
                        },
                        {
                            term: {
                                id: '3'
                            }
                        }
                    ]
                }
            },
            sort      : {
                orderId: 'desc'
            }
        };

        return xoDsHandler.search(removeData).then(function(result) {
            for (var i in result.hits) {
                delete result.hits[i].timestamp;
            }

            result.should.deep.equal(resultTwo);
        });
    });
});
