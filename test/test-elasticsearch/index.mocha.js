/*jshint -W030 */
'use strict';

var Joi            = require('joi'),
    inputMapping   = require('../stubs/inputMapping'),
    XoDsHandler    = require('../../'),
    xoDsHandler    = new XoDsHandler(),
    testDataSource = {
        index: require('../mocks/read-stores').index,
        type : require('../mocks/read-stores').type
    },
    dataHandler    = require('../stubs/esDataHandler')(testDataSource);

require('../lib/chai');

describe('ES: Index ', function() {
    before(function() {
        return dataHandler.insert(xoDsHandler);
    });

    after(function() {
        return dataHandler.removeAll(xoDsHandler);
    });

    /* Tests result objects */
    var resultCreate = {
        errors: false,
        items : [
            {
                index: {
                    _id     : '1',
                    _index  : testDataSource.index,
                    _type   : testDataSource.type,
                    _version: 2,
                    status  : 200
                }
            },
            {
                index: {
                    _id     : '2',
                    _index  : testDataSource.index,
                    _type   : testDataSource.type,
                    _version: 2,
                    status  : 200
                }
            },
            {
                index: {
                    _id     : '3',
                    _index  : testDataSource.index,
                    _type   : testDataSource.type,
                    _version: 2,
                    status  : 200
                }
            }
        ]
    };

    var resultError = 'Error: There is no id in payload data for document number: 0';

    /* Create documents */
    it('should respond: ' + JSON.stringify(resultCreate), function() {
        var createData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            payload      : [
                {
                    id         : '1',
                    name       : 'Test Row 1',
                    description: 'Test description text'
                },
                {
                    id         : '2',
                    name       : 'Test Row 2',
                    description: 'Test description text',
                    location   : {
                        city: 'Austin'
                    }
                },
                {
                    id         : '3',
                    name       : 'Test Row 3',
                    description: 'Test description text',
                    location   : {
                        city: 'Austin'
                    }
                }
            ],
            payloadSchema: {
                id         : Joi.number().required(),
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            logQuery     : true
        };

        return xoDsHandler.index(createData).then(function(result) {
            delete result.took;
            result.should.deep.equal(resultCreate);
        });
    });

    /* Create documents with refresh set to true*/
    it('should create documents with refresh set to true', function() {
        var createData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            payload      : [
                {
                    id         : '4',
                    name       : 'Test Row 4',
                    description: 'Test description text'
                },
                {
                    id         : '5',
                    name       : 'Test Row 5',
                    description: 'Test description text',
                    location   : {
                        city: 'Austin'
                    }
                },
                {
                    id         : '6',
                    name       : 'Test Row 6',
                    description: 'Test description text',
                    location   : {
                        city: 'Austin'
                    }
                }
            ],
            payloadSchema: {
                id         : Joi.number().required(),
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            logQuery     : true,
            refresh      : true
        };
        var resultCreateRefresh = {
            errors: false,
            items : [
                {
                    index: {
                        _id     : '4',
                        _index  : testDataSource.index,
                        _type   : testDataSource.type,
                        _version: 1,
                        status  : 201
                    }
                },
                {
                    index: {
                        _id     : '5',
                        _index  : testDataSource.index,
                        _type   : testDataSource.type,
                        _version: 1,
                        status  : 201
                    }
                },
                {
                    index: {
                        _id     : '6',
                        _index  : testDataSource.index,
                        _type   : testDataSource.type,
                        _version: 1,
                        status  : 201
                    }
                }
            ]
        };

        return xoDsHandler.index(createData)
            .then(function(result) {
                delete result.took;
                result.should.deep.equal(resultCreateRefresh);

                return xoDsHandler.search({
                    dataSource   : testDataSource.index + '.' + testDataSource.type,
                    query        : {
                        match: {
                            id: '6'
                        }
                    }
                });
            })
            .then(function(resultSearch) {
                resultSearch.hits.length.should.equal(1);
                resultSearch.hits[0].id.should.equal('6');
            });
    });

    /* Create documents with refresh set to false*/
    it('should create documents with refresh default to false', function() {
        var createData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            payload      : [
                {
                    id         : '7',
                    name       : 'Test Row 7',
                    description: 'Test description text'
                },
                {
                    id         : '8',
                    name       : 'Test Row 8',
                    description: 'Test description text',
                    location   : {
                        city: 'Austin'
                    }
                },
                {
                    id         : '9',
                    name       : 'Test Row 9',
                    description: 'Test description text',
                    location   : {
                        city: 'Austin'
                    }
                }
            ],
            payloadSchema: {
                id         : Joi.number().required(),
                name       : Joi.string().optional(),
                description: Joi.string().optional(),
                location   : Joi.object().optional(),
                timestamp  : {
                    createdAt: Joi.string().optional(),
                    updatedAt: Joi.string().optional()
                }
            },
            mapping      : inputMapping,
            logQuery     : true
        };
        var resultCreateRefresh = {
            errors: false,
            items : [
                {
                    index: {
                        _id     : '7',
                        _index  : testDataSource.index,
                        _type   : testDataSource.type,
                        _version: 1,
                        status  : 201
                    }
                },
                {
                    index: {
                        _id     : '8',
                        _index  : testDataSource.index,
                        _type   : testDataSource.type,
                        _version: 1,
                        status  : 201
                    }
                },
                {
                    index: {
                        _id     : '9',
                        _index  : testDataSource.index,
                        _type   : testDataSource.type,
                        _version: 1,
                        status  : 201
                    }
                }
            ]
        };

        return xoDsHandler.index(createData)
            .then(function(result) {
                delete result.took;
                result.should.deep.equal(resultCreateRefresh);

                return xoDsHandler.search({
                    dataSource   : testDataSource.index + '.' + testDataSource.type,
                    query        : {
                        match: {
                            id: '7'
                        }
                    }
                });
            })
            .then(function(resultSearch) {
                resultSearch.hits.length.should.equal(0);
            });
    });

    /* Create document with no id - error */
    it('should respond: ' + JSON.stringify(resultCreate), function() {
        var createData = {
            dataSource   : testDataSource.index + '.' + testDataSource.type,
            payload      : {
                name       : 'Test Row 1',
                description: 'Test description text',
                location   : {
                    city: 'Austin'
                }
            }
        };

        return xoDsHandler.index(createData).should.eventually.be.rejectedWith(resultError);
    });
});
