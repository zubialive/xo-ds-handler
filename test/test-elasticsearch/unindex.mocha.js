/*jshint -W030 */
'use strict';

var XoDsHandler    = require('../../'),
    xoDsHandler    = new XoDsHandler(),
    testDataSource = {
        index: require('../mocks/read-stores').index,
        type : require('../mocks/read-stores').type
    },
    dataHandler    = require('../stubs/esDataHandler')(testDataSource);

require('../lib/chai');

describe('ES: Unindex ', function() {
    before(function() {
        return dataHandler.insert(xoDsHandler);
    });

    after(function() {
        return dataHandler.removeAll(xoDsHandler);
    });

    var resultNoPayload = 'Error: There is no query or payload sent to unindex method';

    /* Remove specific documents */
    it('should respond true', function() {
        var searchData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            query     : {
                match_all: {}
            }
        };

        var removeData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            query     : {
                bool: {
                    should: [
                        {
                            term: {
                                id: 2
                            }
                        },
                        {
                            term: {
                                id: 3
                            }
                        }
                    ]
                }
            },
            logQuery  : true
        };

        return xoDsHandler.unindex(removeData).then(function(response) {
            (response).should.be.true;

            return xoDsHandler.search(searchData).then(function(result) {
                (result.total).should.equal(1);
            });
        });
    });

    /* Remove specific documents using payload */
    it('should respond true', function() {
        var searchData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            query     : {
                match_all: {}
            }
        };

        var removeData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            payload   : ['2', '3']
        };

        return xoDsHandler.unindex(removeData).then(function(response) {
            (response).should.be.true;

            return xoDsHandler.search(searchData).then(function(result) {
                (result.total).should.equal(1);
            });
        });
    });

    /* Remove inexistent document */
    it('should respond true', function() {
        var searchData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            query     : {
                match_all: {}
            }
        };

        var removeData = {
            dataSource: testDataSource.index + '.' + testDataSource.type,
            payload   : ['10']
        };

        return xoDsHandler.unindex(removeData).then(function(response) {
            (response).should.be.true;

            return xoDsHandler.search(searchData).then(function(result) {
                (result.total).should.equal(1);
            });
        });
    });

    /* No query or payload sent - error */
    it('should respond true', function() {
        var removeData = {
            dataSource: testDataSource.index + '.' + testDataSource.type
        };

        return xoDsHandler.unindex(removeData).should.eventually.be.rejectedWith(resultNoPayload);
    });
});
