'use strict';

var XoDsHandler    = require('../../'),
    xoDsHandler    = new XoDsHandler(),
    testDataSource = {
        index: require('../mocks/read-stores').index,
        type : require('../mocks/read-stores').type
    },
    dataHandler    = require('../stubs/esDataHandler')(testDataSource);

require('../lib/chai');

describe('ES: Es ', function() {
    before(function() {
        return dataHandler.insert(xoDsHandler);
    });

    after(function() {
        return dataHandler.removeAll(xoDsHandler);
    });

    /* Index document with es method */
    var result = {
        _id     : '4',
        _index  : 'users',
        _type   : 'users',
        _version: 1,
        created : true
    };

    it('should respond: ' + JSON.stringify(result), function() {
        var query = {
            index: testDataSource.index,
            type : testDataSource.type,
            id   : '4',
            body : {
                id         : 4,
                name       : 'Test Row 4',
                description: 'Test description text',
                orderId    : 4
            }
        };

        return xoDsHandler.es(testDataSource.index, 'index', query, {
            logQuery: process.env.DEBUG_HANDLER_QUERIES
        }).then(function(result) {
            result.should.deep.equal(result);
        });
    });

    /* Try invalid action */
    it('should respond: Error: Invalid action passed to es method', function() {
        return xoDsHandler.es(testDataSource.index, 'invalid-action', {}).should.be.rejectedWith('Error: Invalid action passed to es method');
    });

    /* Catch elasticSearch error */
    it('should respond: TypeError: Unable to build a path with those params. Supply at least index, type', function() {
        var query = {
            type: testDataSource.type,
            body: {
                name: 'Test Row 4'
            }
        };

        return xoDsHandler.es(testDataSource.index, 'index', query).catch(function(error) {
            error.should.be.equal('TypeError: Unable to build a path with those params. Supply at least index, type');
        });
    });
});
