'use strict';

var config = {
    host  : 'esbox',
    port  : 9200,
    user  : '',
    pass  : '',
    index : 'users',
    type  : 'users',
    stores: {}
};

if (!config.user && !config.pass) {
    config.stores[config.index] = 'http://' + config.user + ':' + config.pass + '@' + config.host + ':' + config.port;
} else {
    config.stores[config.index] = 'http://' + config.host + ':' + config.port;
}

module.exports = config;
