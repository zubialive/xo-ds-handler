'use strict';

var config = {
    protocol : 'postgres',
    host     : 'pgbox',
    port     : 5432,
    dbName   : 'testdb',
    user     : 'postgres',
    pass     : 'password',
    tableName: 'testtable',
    stores   : {}
};

config.stores.postgres       = config.protocol + '://' + config.user + ':' + config.pass + '@' + config.host + ':' + config.port + '/postgres';
config.stores[config.dbName] = config.protocol + '://' + config.user + ':' + config.pass + '@' + config.host + ':' + config.port + '/' + config.dbName;

module.exports = config;
